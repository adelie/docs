<?xml version="1.0" encoding="utf-8"?>
<chapter label="6" id="security">
    <title>Security</title>
    <highlights><para>In this chapter, we will review best practices for keeping your Adélie Linux systems secure.</para></highlights>
    <section id="introduction6">
        <title>Introduction to computer security</title>
        <para>No one book can teach you everything about computer security.  Computer security is not simply an item on a list that must be done once and then it is "completed".  Computers are very powerful tools, and just as they can be used to teach, create, and produce, they can also be used for more nefarious purposes.  By taking a proactive approach to computer security, and treating it as a process, you will be much more successful in keeping the security, confidentiality, and integrity of your data intact.  You first need to identify your <firstterm>threat model</firstterm>; that is, what adversaries are you attempting to keep out?  A sophisticated nation-state requires a much different security plan than a random hacker.</para>
        <para>The suggestions in this handbook are a starting point to help you develop an actionable plan to keep your computer secure.  They are by no means exhaustive.  Remember to always keep learning; knowledge is power.</para>
    </section>
    <section id="apk_sec">
        <title>APK</title>
        <itemizedlist>
            <listitem><para>Only use repositories and mirrors with HTTPS.</para></listitem>
            <listitem><para>Run system updates whenever it is convenient.</para></listitem>
            <listitem><para>Ensure any custom or third-party repositories have high-grade signature keys and that you trust the developers.</para></listitem>
        </itemizedlist>
    </section>
    <section id="service_sec">
        <title>Services</title>
        <itemizedlist>
            <listitem><para>If a service is only being used in your internal network, make sure it is configured to only accept connections from that network.  This reduces the chance that an external attacker from the Internet can connect to it.</para></listitem>
            <listitem><para>Keep track of best practices for the services you run.  The service's documentation is likely to contain good advice for security.</para></listitem>
        </itemizedlist>
    </section>
</chapter>