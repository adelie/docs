.POSIX:

all: out/html/devel/index.html out/text/develbook.txt \
	out/html/install/index.html out/text/installbook.txt \
	out/html/porting/index.html out/text/portguide.txt \
	out/html/admin/index.html out/text/adminbook.txt \
	out/html/hcl/index.html out/text/hclbook.txt

image:
	docker build . -t cookbook
site:
	docker run -u$(shell id -u):$(shell id -g) -v$(CURDIR):/tmp -w /tmp --rm -it cookbook make

out/text/adminbook.txt: src/admin/*.xml
	@mkdir -p out/text
	@printf 'Administrator Handbook: Generating text...\n'
	@xmlto txt -o out/text src/admin/adminbook.xml

out/html/admin/index.html: src/admin/*.xml src/pretty.css
	@mkdir -p out/html/admin
	@printf 'Administrator Handbook: Generating HTML...\n'
	@xmlto html -m master.xsl -o out/html/admin src/admin/adminbook.xml
	@cp src/pretty.css out/html/admin/

out/text/develbook.txt: src/devel/*.xml
	@mkdir -p out/text
	@printf 'Developer Handbook: Generating text...\n'
	@xmlto txt -o out/text src/devel/develbook.xml

out/html/devel/index.html: src/devel/*.xml src/pretty.css
	@mkdir -p out/html/devel
	@printf 'Developer Handbook: Generating HTML...\n'
	@xmlto html -m master.xsl -o out/html/devel src/devel/develbook.xml
	@cp src/pretty.css out/html/devel/

out/text/hclbook.txt: src/hcl/*.xml
	@mkdir -p out/text
	@printf 'Hardware Compatibility List: Generating text...\n'
	@xmlto txt -o out/text src/hcl/hclbook.xml

out/html/hcl/index.html: src/hcl/*.xml
	@mkdir -p out/html/hcl
	@printf 'Hardware Compatibility List: Generating HTML...\n'
	@xmlto html -m master.xsl -o out/html/hcl src/hcl/hclbook.xml
	@cp src/pretty.css out/html/hcl/

out/text/installbook.txt: src/install/*.xml
	@mkdir -p out/text
	@printf 'Installation Handbook: Generating text...\n'
	@xmlto txt -o out/text src/install/installbook.xml

out/html/install/index.html: src/install/*.xml src/pretty.css
	@mkdir -p out/html/install
	@printf 'Installation Handbook: Generating HTML...\n'
	@xmlto html -m master.xsl -o out/html/install src/install/installbook.xml
	@cp src/pretty.css out/html/install/

out/text/portguide.txt: src/porting/*.xml
	@mkdir -p out/text
	@printf 'Porting Guide: Generating text...\n'
	@xmlto txt -o out/text src/porting/portguide.xml

out/html/porting/index.html: src/porting/*.xml src/pretty.css
	@mkdir -p out/html/porting
	@printf 'Porting Guide: Generating HTML...\n'
	@xmlto html -m master.xsl -o out/html/porting src/porting/portguide.xml
	@cp src/pretty.css out/html/porting/

install:
	@mkdir -p "${DESTDIR}"/usr/share/doc/adelie-handbook/
	@cp -r out/* "${DESTDIR}"/usr/share/doc/adelie-handbook/
